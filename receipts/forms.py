from django import forms
from .models import Receipt, ExpenseCategory, Account
#from django.contrib.auth.mixins import LoginRequiredMixin
#from django.views.generic import ListView


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']


class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ['name']


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ['name', 'number']


#class ExpenseCategoryListView(LoginRequiredMixin, ListView):
#         #model = ExpenseCategory
#         context_object_name = 'categories'
#         template_name = 'receipts/expense_category_list.html'

#         def get_queryset(self):
#             return ExpenseCategory.objects.filter(user=self.request.user).annotate(num_receipts=Count('receipt'))

#         def get_context_data(self, **kwargs):
#             context = super().get_context_data(**kwargs)
#             total_receipts = Receipt.objects.filter(user=self.request.user).count()
#             context['total_receipts'] = total_receipts
#             return context




# #class AccountListView(LoginRequiredMixin, ListView):
#         model = Account
#         context_object_name = 'accounts'
#         template_name = 'receipts/account_list.html'

#         def get_queryset(self):
#             return Account.objects.filter(user=self.request.user).annotate(num_receipts=Count('receipt'))

#         def get_context_data(self, **kwargs):
#             context = super().get_context_data(**kwargs)
#             total_receipts = Receipt.objects.filter(user=self.request.user).count()
#             context['total_receipts'] = total_receipts
#             return context
