from django.shortcuts import render, redirect
from .models import Receipt, Account
from django.contrib.auth.decorators import login_required
from .forms import ExpenseCategoryForm, ReceiptForm, AccountForm
from django.views.generic.edit import CreateView
from django.utils.decorators import method_decorator

@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, 'receipts/receipt_list.html', {'receipts': receipts})


# #class ReceiptListView():
#      model = Receipt
#      template_name = 'receipts/receipt_list.html'
#      context_object_name = 'receipts'


def get_queryset(self):
    queryset = super().get_queryset()
    return Receipt.objects.filter(purchaser=self.request.user)


def form_valid(self, form):
    form.instance.purchaser = self.request.user
    return super().form_valid(form)


# @login_required
# def create_receipt(request):
#     if request.method == 'POST':
#         form = ReceiptForm(request.POST)
#         if form.is_valid():
#             receipt = form.save(commit=False)
#             receipt.purchaser = request.user
#             receipt.save()
#             return redirect('home')
#     else:
#         form = ReceiptForm()
#     return render(request, 'receipts/create_receipt.html', {'form': form})





@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()
    return render(request, 'receipts/create_category.html', {'form': form})


@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('home')
    else:
        form = AccountForm()
    return render(request, 'receipts/create_account.html', {'form': form})


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    return render(request, 'receipts/create_receipt.html', {'form': form})

@login_required
def account_list(request):
    account = Account.objects.filter(purchaser=request.user)
    return render(request, 'receipts/account_list.html', {'accounts': account})
