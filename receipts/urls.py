from django.urls import path
from .views import receipt_list, create_account, create_receipt, account_list


urlpatterns = [
    path('categories/', account_list, name='account_list'),
    path('accounts/create/', create_account, name='create_account'),
    path('create/', create_receipt, name='create_receipt'),
    path('', receipt_list, name='home'),
]
